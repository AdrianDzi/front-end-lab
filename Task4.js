document.write("<p>Task 4</p>");

var array = [6, 2, 3, 7, 5, 4, 1, 4, 1, 6, 7, 8345, 345, 234, 23, 654, 8, 4, -123, -3424];

function bubble(arr) {
    var len = arr.length;
    for (let j = 0; j < len; j++) {
        for (let i = 0; i < len; i++) {
            if (arr[i] < arr[i + 1]) {
                var temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    return arr;
}

document.write("Before sorting: " + array + "<br>");
document.write("After sorting: " + bubble(array));